import React from 'react';

const About = () => {
  return (
    <section
      className='about container'
      id='about'
      data-aos='fade-right'
      data-aos-easing='ease-in-out'
      data-aos-duration='2000'
    >
      <div className='about__title'>
        <p className='category-title'>WHY US? VALUES WE BRING</p>
        <p className='category-title-sub'>Our Process</p>
      </div>

      <div className='about__content'>
        <div className='about__content-item'>
          <div className='about__content-list-img box-shadow'>
            <div className='about__content-item-img'>
              <img src='/images/Standard.png' alt='' />
            </div>
            <p>Team Building</p>
          </div>
          <h5>Daily Standup</h5>
          <p>Amsterdams progessive multicultural conscientious</p>
        </div>
        <div className='about__content-item'>
          <div className='about__content-list-img box-shadow'>
            <p>1-1 Meeting with Mr.Zordar Smt</p>
            <div className='about__content-item-img'>
              <img src='/images/Standard.png' alt='' />
              <img src='/images/Standard.png' alt='' />
            </div>
          </div>
          <h5>Weekly Syn. Meeting</h5>
          <p>Amsterdams progessive multicultural conscientious</p>
        </div>
        <div className='about__content-item'>
          <div className='about__content-list-img box-shadow'>
            <p>Daily Standup Notes for...</p>
            <div className='about__content-item-img'>
              <img src='/images/Standard.png' alt='' />
              <img src='/images/Standard.png' alt='' />
            </div>
          </div>
          <h5>User Testing</h5>
          <p>Amsterdams progessive multicultural conscientious</p>
        </div>
      </div>
    </section>
  );
};

export default About;
