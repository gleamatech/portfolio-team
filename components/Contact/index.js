import React from 'react';
import EvaIcon from '../common/EvaIcon';

const Contact = () => {
  return (
    <footer
      id='email'
      className='contact'
      data-aos='fade-right'
      data-aos-easing='ease-in-out'
      data-aos-duration='2000'
    >
      <div className='container'>
        <div className='contact__box box-shadow'>
          <div className='contact__content'>
            <p className='category-title'>NEWS</p>
            <p className='category-title-sub'>Contact us</p>
          </div>
          <div className='contact__form'>
            <form action='' className='form'>
              <input
                type='text'
                className='input contact-input'
                placeholder='Enter your full name'
              />
              <input
                type='text'
                className='input contact-input'
                placeholder='Enter email address'
              />
              <input type='select' className='input contact-input' placeholder='Your Budget' />
            </form>
            <button className='btn'>Send</button>
          </div>
        </div>
        <div className='copyright'>
          <p>COPYRIGHT@GLEATECH.COM</p>
        </div>
      </div>
    </footer>
  );
};

export default Contact;
