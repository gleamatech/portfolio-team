import React from 'react';
import EvaIcon from '../common/EvaIcon';

const Banner = () => {
  return (
    <section
      className='banner container'
      id='banner'
      data-aos='fade-left'
      data-aos-easing='ease-in-out'
      data-aos-duration='2000'
    >
      <div className='banner__image'>
        <img src='/images/home.png' alt='' />
      </div>
      <div className='banner__content'>
        <p className='category-title'>INTRODUCTION</p>
        <h1>
          <span>Hello Digital</span> Design
        </h1>
        <a href='#' className='btn'>
          <div className='banner-btn'>
            <p>See Our Works</p>
            <EvaIcon name='chevron-right-outline' color='#fff' />
          </div>
        </a>
      </div>
      <a className='down-other-section' href='#about'>
        <EvaIcon name='arrow-downward-outline' color='#888' height={20} width={20} />
      </a>
    </section>
  );
};

export default Banner;
