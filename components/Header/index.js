import React from 'react';
import EvaIcon from '../common/EvaIcon';

const Header = () => {
  return (
    <header
      className='header'
      data-aos='fade-left'
      data-aos-easing='ease-in-out'
      data-aos-duration='2000'
    >
      <nav className='header__menu'>
        <ul className='header__menu-list'>
          <li className='header__menu-item'>
            <a href='#banner'>Gleamatech</a>
          </li>
          <li className='header__menu-item'>
            <a href='#about'>Our services</a>
          </li>
          <li className='header__menu-item'>
            <a href='#case-study'>Our portfolio</a>
          </li>
          <li className='header__menu-item'>
            <a href='#team'>About us</a>
          </li>
          <li className='header__menu-item'>
            <a href='#email'>
              <span>MAIL</span>
              CONTACT US AT: WORK@GLEAMATECH.COM
            </a>
          </li>
          <li>
            <a href='#email'>Say hi!</a>
          </li>
        </ul>
      </nav>
      {/* Start menu mobile */}
      <div className='logo'>
        <a href='#banner'>Gleamatech</a>
      </div>

      <label htmlFor='nav__mobile__input' className='toggle-icon'>
        <EvaIcon name='menu-outline' color='#1e3547' />
      </label>

      <input type='checkbox' className='nav__input' id='nav__mobile__input' />
      <label htmlFor='nav__mobile__input' className='header__nav-overlay'></label>

      <nav className='header__menu__mobile'>
        <ul className='header__menu__mobile-list'>
          <label htmlFor='nav__mobile__input' className='header__menu__mobile-close'>
            <EvaIcon name='close-outline' color='#1e3547' width={24} height={24} />
          </label>
          <li className='header__menu__mobile-item'>
            <EvaIcon name='bulb-outline' color='#1e3547' width={24} height={24} />
            <a href='#about'>Our services</a>
          </li>
          <li className='header__menu__mobile-item'>
            <EvaIcon name='checkmark-circle-2-outline' color='#1e3547' width={24} height={24} />
            <a href='#case-study'>Our portfolio</a>
          </li>
          <li className='header__menu__mobile-item'>
            <EvaIcon name='person-done-outline' color='#1e3547' width={24} height={24} />
            <a href='#team'>About us</a>
          </li>
          <li className='header__menu__mobile-item'>
            <a href='#email'>
              <span>MAIL</span> Work@glematech.com
            </a>
          </li>
        </ul>
      </nav>
      {/* End menu mobile */}
    </header>
  );
};

export default Header;
