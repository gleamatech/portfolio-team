import React from 'react';

const CaseStudy = () => {
  return (
    <section
      className='study'
      id='case-study'
      data-aos='fade-left'
      data-aos-easing='ease-in-out'
      data-aos-duration='2000'
    >
      <div className='study__inner container'>
        <div className='study__image'>
          <img src='/images/Introduction.svg' alt='' />
        </div>
        <div className='study__content'>
          <p className='category-title'>OUR TECHNOLOGIES</p>
          <h3 className='category-title-sub'>Pay Smartly on Grabe</h3>
          <p className='study__content-p'>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi laudantium omnis animi
            molestiae veniam dolore ab itaque cumque.
          </p>
          <div className='study__content-link'>
            <a href='#'>
              <img src='/images/appstore.png' alt='' />
            </a>
            <a href='#'>
              <img src='/images/googleplay.png' alt='' />
            </a>
          </div>
        </div>
      </div>
    </section>
  );
};

export default CaseStudy;
