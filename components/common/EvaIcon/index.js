import React, { useEffect } from 'react';
import * as eva from 'eva-icons';

const EvaIcon = ({ name, color, height, width, className }) => {
  useEffect(() => {
    eva.replace();
  }, []);

  return (
    <span width={width} height={height}>
      <i data-eva={name} data-eva-fill={color} data-eva-height={height} data-eva-width={width}></i>
    </span>
  );
};

export default EvaIcon;
