import React from 'react';
import EvaIcon from '../common/EvaIcon';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const News = () => {
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    adaptiveHeight: true
  };

  return (
    <section
      className='news container'
      id='new'
      data-aos='fade-left'
      data-aos-easing='ease-in-out'
      data-aos-duration='2000'
    >
      <div className='news__content'>
        <div className='news__email'>
          <p className='category-title'>OUR MANAGEMENT PROCESS</p>
          <h3 className='category-title-sub'>Lastest News</h3>
          <p className='news__email-content'>Join our newsletter to stay update</p>
          <form method='#' className='news-join'>
            <input type='text' className='news__address input' placeholder='Email address' />
            <button className='btn news-btn'>join</button>
          </form>
        </div>

        <Slider {...settings} className='news__posts'>
          <div className='news__posts-item'>
            <div className='news__posts-img'>
              <img src='/images/personal.png' className='news-img-post' alt='' />
              <div className='img-category'>
                <span>Agency</span>
                <span>Portfolio</span>
              </div>
            </div>
            <div className='news__posts-content box-shadow'>
              <p>23 jan 2018</p>
              <h5>Frist time Blogger Entrepreneurs How to increase the...</h5>
              <div className='news__posts-author'>
                <img src='/images/personal2.png' className='news-img-avatar' alt='' />
                <u>By Ryan Bettins</u>
                <a href='' className='btn '>
                  <EvaIcon name='paper-plane-outline' color='#fff' width={24} height={24} />
                </a>
              </div>
            </div>
          </div>

          <div className='news__posts-item'>
            <div className='news__posts-img'>
              <img src='/images/personal.png' className='news-img-post' alt='' />
              <div className='img-category'>
                <span>Agency</span>
                <span>Portfolio</span>
              </div>
            </div>
            <div className='news__posts-content box-shadow'>
              <p>23 jan 2018</p>
              <h5>Frist time Blogger Entrepreneurs How to increase the...</h5>
              <div className='news__posts-author'>
                <img src='/images/personal2.png' className='news-img-avatar' alt='' />
                <u>By Ryan Bettins</u>
                <a href='' className='btn '>
                  <EvaIcon name='paper-plane-outline' color='#fff' width={24} height={24} />
                </a>
              </div>
            </div>
          </div>

          <div className='news__posts-item'>
            <div className='news__posts-img'>
              <img src='/images/personal.png' className='news-img-post' alt='' />
              <div className='img-category'>
                <span>Agency</span>
                <span>Portfolio</span>
              </div>
            </div>
            <div className='news__posts-content box-shadow'>
              <p>23 jan 2018</p>
              <h5>Frist time Blogger Entrepreneurs How to increase the...</h5>
              <div className='news__posts-author'>
                <img src='/images/personal2.png' className='news-img-avatar' alt='' />
                <u>By Ryan Bettins</u>
                <a href='' className='btn '>
                  <EvaIcon name='paper-plane-outline' color='#fff' width={24} height={24} />
                </a>
              </div>
            </div>
          </div>
        </Slider>
      </div>
    </section>
  );
};

export default News;
