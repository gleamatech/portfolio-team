import React from 'react';
import EvaIcon from '../common/EvaIcon';

const Team = () => {
  return (
    <section
      className='team container'
      id='team'
      data-aos='fade-right'
      data-aos-easing='ease-in-out'
      data-aos-duration='2000'
    >
      <div className='team__content box-shadow'>
        <div className='team__info '>
          <img src='/images/comment.svg' alt='' />
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni quod necessitatibus error
            assumenda porro delectus, tempore nobis iure debitis tenetur quisquam autem numquam
            animi est. Modi rerum expedita reprehenderit qui?
          </p>
          <b>Ricky Walton</b>
          <p>Marketing Manager & CMO</p>
        </div>
        <div className='team__direction'>
          <div className='team__direction-web'>
            <img src='/images/camera.svg' alt='' />
            <b>appear.in</b>
          </div>

          <div className='team__direction-back-forward'>
            <a className='btn btn-rounded'>
              <EvaIcon name='arrow-ios-back-outline' color='#fff' width={24} height={24} />
            </a>
            <a className='btn btn-rounded'>
              <EvaIcon name='arrow-ios-forward-outline' color='#fff' width={24} height={24} />
            </a>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Team;
