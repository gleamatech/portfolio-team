import '../styles/globals.scss';
import { useEffect } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import dynamic from 'next/dynamic';

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    AOS.init({
      useClassNames: true,
      initClassName: true
    });
  }, []);
  return <Component {...pageProps} />;
}
// export default MyApp;

export default dynamic(() => Promise.resolve(MyApp), { ssr: false });
